<!doctype html>
<html>
	<head>
		<title>Test postulación desarrollador backend junior | Héctor Pérez Silva</title>
		<link rel="stylesheet" href="css/bootstrap.min.css"/>
	</head>
	<body>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
						
						<div class="col-xs-12 text-center">
							<h3>Test postulación desarrollador backend junior</h3>
						</div>
						
						<div class="col-lg-12">
							<form id="formularioFecha" action="php/fecha.php" method="post">
								<div class="form-group col-lg-5 col-lg-offset-1">
									<label for="fecha1">Fecha 1</label>
									<input class="form-control" type="date" id="fecha1" name="fecha1" >
								</div>
								<div class="form-group  col-lg-5">
									<label for="numero1">Numero 1</label>
									<input class="form-control" type="number" id="numero1" name="numero1" min="0">
								</div>
								<div class="form-group col-lg-5 col-lg-offset-1">
									<label for="fecha2">Fecha 2</label>
									<input class="form-control" type="date" id="fecha2" name="fecha2" >
								</div>
								<div class="form-group  col-lg-5">
									<label for="numero2">Numero 2</label>
									<input class="form-control" type="number" id="numero2" name="numero2" min="0">
								</div>
								<div class="form-group col-lg-5 col-lg-offset-1">
									<label for="fecha3">Fecha 3</label>
									<input class="form-control" type="date" id="fecha3" name="fecha3" >
								</div>
								<div class="form-group  col-lg-5">
									<label for="numero3">Numero 3</label>
									<input class="form-control" type="number" id="numero3" name="numero3" min="0">
								</div>
								<div class="form-group col-lg-5 col-lg-offset-1">
									<label for="fecha4">Fecha 4</label>
									<input class="form-control" type="date" id="fecha4" name="fecha4" >
								</div>
								<div class="form-group  col-lg-5">
									<label for="numero4">Numero 4</label>
									<input class="form-control" type="number" id="numero4" name="numero4" min="0">
								</div>
								<div class="form-group col-lg-5 col-lg-offset-1">
									<input type="submit" class="btn btn-success" value="Calcular">
								</div>
								<div class="form-group col-lg-10 col-lg-offset-1">
									<div id="resultado"></div>
								</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</section>
		
			
		<script src="js/jquery-3.2.1.min.js"></script>	
		<script src="js/bootstrap.min.js"></script>	
		<script>
		
			// al hacer click en el boton calcular, se ejecuta el codigo jquery
			$('#formularioFecha').on('submit', function(){
				
				// se crean variables con los valores de las fechas y números
				var fecha1 = $('#fecha1').val();
				var fecha2 = $('#fecha2').val();
				var fecha3 = $('#fecha3').val();
				var fecha4 = $('#fecha4').val();
				var numero1 = $('#numero1').val();
				var numero2 = $('#numero2').val();
				var numero3 = $('#numero3').val();
				var numero4 = $('#numero4').val();
				
				// se crean variables de las fechas en timestamp, se debe utlizar la divisón en 1000, porque Date.parse establece
				// la fecha en milisegundos, no en segundos
				var fecha1aComparar = Date.parse(fecha1)/1000;
				var fecha2aComparar = Date.parse(fecha2)/1000;
				var fecha3aComparar = Date.parse(fecha3)/1000;
				var fecha4aComparar = Date.parse(fecha4)/1000;
				
				// se revisa que las fechas y sus respectivos números no estén vacíos
				if(fecha1 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">La fecha n° 1 está vacía</div>').fadeIn(700);
					$('#fecha1').focus();
				}
				else if(fecha2 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">La fecha n° 2 está vacía</div>').fadeIn(700);
					$('#fecha2').focus();
				}
				else if(fecha3 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">La fecha n° 3 está vacía</div>').fadeIn(700);
					$('#fecha3').focus();
				}
				else if(fecha4 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">La fecha n° 4 está vacía</div>').fadeIn(700);
					$('#fecha4').focus();
				}
				else if(numero1 < 0 || numero1 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">El número 1 debe ser mayor o igual a 0</div>').fadeIn(700);
					$('#numero1').focus();
				}
				else if(numero2 < 0 || numero2 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">El número 2 debe ser mayor o igual a 0</div>').fadeIn(700);
					$('#numero2').focus();
				}
				else if(numero3 < 0 || numero3 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">El número 3 debe ser mayor o igual a 0</div>').fadeIn(700);
					$('#numero3').focus();
				}
				else if(numero4 < 0 || numero4 === ''){
					$('#resultado').hide().html('<div class="alert alert-danger">El número 4 debe ser mayor o igual a 0</div>').fadeIn(700);
					$('#numero4').focus();
				}
				// se revisa que la fecha 2 sea mayor que la fecha 1
				else if(fecha1aComparar >= fecha2aComparar){
					$('#resultado').hide().html('<div class="alert alert-danger">La Fecha n° 2 debe ser mayor a la fecha 1</div>').fadeIn(700);
					$('#fecha2').focus();
				}
				// se revisa que la fecha 3 sea mayor que la fecha 2
				else if(fecha2aComparar >= fecha3aComparar){
					$('#resultado').hide().html('<div class="alert alert-danger">La Fecha n° 3 debe ser mayor a la fecha 2</div>').fadeIn(700);
					$('#fecha3').focus();
				}
				// se revisa que la fecha 4 sea mayor que la fecha 3
				else if(fecha3aComparar >= fecha4aComparar){
					$('#resultado').hide().html('<div class="alert alert-danger">La Fecha n° 4 debe ser mayor a la fecha 3</div>').fadeIn(700);
					$('#fecha4').focus();
				}
				else{
					
					// si todas las validaciones han sido exitosas, se procede a crear las variables para el metodo ajax
					
					// variable url corresponde a la dirección donde se enviaran los datos
					// variable type corresponde al metodo de envio de datos, en este caso post
					// variable data corresponde a todos los datos contenidos en campos dentro del formulario
					
					var url = $('#formularioFecha').attr('action'),
						type = $('#formularioFecha').attr('method'),
						data = $('#formularioFecha').serializeArray();
						
						// se llama al metod ajax de jquery estableciendo sus parametros minimos
						$.ajax({
							
							url: url,
							type: type,
							data: data,
							
							//success define lo que el metodo ajax hara cuando la llamada al servidor sea exitosa
							success: function(data){
								$('#resultado').hide().html(data).fadeIn(700);
							},
							//error define cualquier error o fallo de la llamada
							error: function(){
								$('#resultado').hide().html('Error').fadeIn(700);
							}
							
						});
				}
				
				//return false es para evitar que el formulario se envie, permaneciendo en la misma página, y comunicandose con el servidor
				return false;
			});
		
		</script>	
	</body>
</html>