<?php

	//función para agregar días hábiles a la fecha, con 3 parámetros:
	//1 - $numero representa el numero de la fecha (fecha 1, fecha 2, fecha 3, fecha 4, etc...)
	//2 - $fecha representa la fecha ingreada por el usuario
	//3 - $cantidad representa la cantidad de días a agregar
	
	function agregarDiasHabiles($numero, $fecha, $cantidad){
		
		//crear un objeto DateTime con la fecha
		$d = new DateTime( $fecha );
		//obtener el timestamp de la fecha 
		$t = $d->getTimestamp();

		// bucle for para los X días que se quieran añadir
		for($i=0; $i<$cantidad; $i++){

			// agregar un día al timestamp, debe ser ingreado en segundos
			$agregarDia = 86400;

			// se obtiene el día siguiente
			$diaSiguiente = date('w', ($t+$agregarDia));

			// si el día siguiente es sábado o domingo, se resta 1 día, ya que el fin de semana no se considera habil
			if($diaSiguiente == 0 || $diaSiguiente == 6) {
				$i--;
			}

			// se modifica el timestamp, agregando 1 día
			$t = $t+$agregarDia;
		}
		// se establece el nuevo timestamp
		$d->setTimestamp($t);
		
		// se muestra el resultado del calculo, formateando las fechas(dia-mes-año)
		echo 'Fecha '.$numero.': '.date('d-m-Y', strtotime($fecha)).'<br>';
		echo 'Días Hábiles a Sumar: '.$cantidad.'<br>';
		echo 'Fecha Calculada: '.$d->format( 'd-m-Y' ). "<br>";
		echo '<br>';
		
	}	