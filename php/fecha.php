<?php

//se revisa que este presente el metodo POST, de lo contrario, el servidor no ejecutará ninguna acción
if($_POST){
	
	
	// se revisan que los datos recibidos no estén vacíos
	if(empty($_POST['fecha1'])){
		echo '<div class="alert alert-danger">La Fecha n° 1 está vacía</div>';
	}
	elseif(empty($_POST['fecha2'])){
		echo '<div class="alert alert-danger">La Fecha n° 2 está vacía</div>';
	}
	elseif(empty($_POST['fecha3'])){
		echo '<div class="alert alert-danger">La Fecha n° 3 está vacía</div>';
	}
	elseif(empty($_POST['fecha4'])){
		echo '<div class="alert alert-danger">La Fecha n° 4 está vacía</div>';
	}
	
	else{
		
		//se crean variables con los datos recibidos
		$fecha1 = $_POST['fecha1'];
		$numero1 = (isset($_POST['numero1']) && !empty($_POST['numero1']))?$_POST['numero1']:0;
					
		
		$fecha2 = $_POST['fecha2'];
		$numero2 = (isset($_POST['numero2']) && !empty($_POST['numero2']))?$_POST['numero2']:0;
		
		$fecha3 = $_POST['fecha3'];
		$numero3 = (isset($_POST['numero3']) && !empty($_POST['numero3']))?$_POST['numero3']:0;
		
		$fecha4 = $_POST['fecha4'];
		$numero4 = (isset($_POST['numero4']) && !empty($_POST['numero4']))?$_POST['numero4']:0;
		
		// se convierten las fechas en timestamp, para poder compararlas
		$fecha1aComparar = strtotime($fecha1);
		$fecha2aComparar = strtotime($fecha2);
		$fecha3aComparar = strtotime($fecha3);
		$fecha4aComparar = strtotime($fecha4);
		
		// se revisa que la fecha 2 sea mayor a la fecha 1
		if($fecha1aComparar >= $fecha2aComparar){
			
			echo '<div class="alert alert-danger">La Fecha n° 2 debe ser mayor a la fecha 1</div>';
		}
		// se revisa que la fecha 3 sea mayor a la fecha 2
		elseif($fecha2aComparar >= $fecha3aComparar){
			
			echo '<div class="alert alert-danger">La Fecha n° 3 debe ser mayor a la fecha 2</div>';
			
		}
		// se revisa que la fecha 4 sea mayor a la fecha 3
		elseif($fecha3aComparar >= $fecha4aComparar){
			
			echo '<div class="alert alert-danger">La Fecha n° 4 debe ser mayor a la fecha 3</div>';
			
		}
		else{
			
			// si todas las validaciones han sido exitosas se incluye el archivo que contiene la función
			
			include_once('../funcion/funcion.php');
			
			// se llama a la funcion agregarDiasHabiles() 4 veces, con las 4 fechas y sus respectivos números
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(1, $fecha1, $numero1);
			echo '</div>';
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(2, $fecha2, $numero2);
			echo '</div>';
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(3, $fecha3, $numero3);
			echo '</div>';
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(4, $fecha4, $numero4);
			echo '</div>';
			
		}
	}
}	