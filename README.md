Ver Proyecto de postulación

1 - clonar el repositorio (postulacion-Hector-Perez) por ssh (git@bitbucket.org:hectorperezsilva/postulacion-hector-perez.git)
o https (https://hectorperezsilva@bitbucket.org/hectorperezsilva/postulacion-hector-perez.git),o descargar 
directamente (https://bitbucket.org/hectorperezsilva/postulacion-hector-perez/get/088053c5b528.zip)

2 - copiar la carpeta resultante y pegar en el directorio de publicación, ya sea public_html, htdocs, www, etc.

3 - Ejecutar el proyecto accediendo al archivo index.php

4 - Es necesario contar con el modulo Apache, ya que el proyecto está escrito en php

